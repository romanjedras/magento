<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 2017-10-03
 * Time: 12:47
 */
class Rjs_Subregistry_Model_Mysql4_Subscribe_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('rjs_subregistry/subscribe');
        parent::_construct();
    }
}