<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 2017-10-03
 * Time: 12:41
 */

class Rjs_Subregistry_Model_Subscribe extends Mage_Core_Model_Abstract
{
    public function __construct()
    {
        $this->_init('rjs_subregistry/subscribe');
        parent::_construct();
    }
    public function updateRegistryData($data)
    {
        try{
            if(!empty($data))
            {
                $this->setProductId($data['product_id']);
                $this->setSubscribeEmail($data['email']);
                $this->setAddedAt(date('Y-m-h'));
            }else{
                throw new Exception("Error Processing Request: Insufficient Data Provided");
            }
        } catch (Exception $e){
            Mage::logException($e);
        }
        echo "<pre>"; print_r($this); echo "</pre>";
        //return $this;
    }
}