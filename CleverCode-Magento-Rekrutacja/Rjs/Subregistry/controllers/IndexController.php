<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 2017-10-03
 * Time: 14:42
 */
class Rjs_Subregistry_IndexController extends Mage_Core_Controller_Front_Action
{

//    public function preDispatch()
//    {
//        parent::preDispatch();
//        if (!Mage::getSingleton('customer/session')->authenticate($this)) {
//            $this->getResponse()->setRedirect(Mage::helper('customer')->getLoginUrl());
//            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
//        }
//    }

    public function indexAction(){
        $this->loadLayout();
        $this->renderLayout();

    }


    public function newAction()
    {
        $this->loadLayout();
        $this->renderLayout();
        return $this;
    }
    public function editAction()
    {
        $this->loadLayout();
        $this->renderLayout();
        return $this;
    }

    public function emailAction(){
        $this->loadLayout();

        $this->renderLayout();
        return $this;
    }

    public function listAction(){
        $this->loadLayout();

        $this->renderLayout();
        return $this;
    }

	public function newEmailAction()
    {
        if (!empty($_POST)) {
            $data = $this->getRequest()->getParams();
            if ($this->getRequest()->getPost() && !empty($data)) {
                $subscribe = Mage::getModel('rjs_subregistry/subscribe');
                $subscribe->updateRegistryData($data);
                $subscribe->save();
                $successMessage = Mage::helper('rjs_subregistry')->__('Subskrypcja zapisana.');
                Mage::getSingleton('core/session')->addSuccess($successMessage);
                $this->_redirectReferer();
            } else {
                throw new Exception("Nie podano wszystkich wymaganych danych.");
            }

        }

    }


}