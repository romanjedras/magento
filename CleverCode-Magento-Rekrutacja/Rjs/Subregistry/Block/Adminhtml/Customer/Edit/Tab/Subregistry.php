<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 2017-10-05
 * Time: 16:08
 */
class Rjs_Subregistry_Block_Adminhtml_Customer_Edit_Tab_Subregistry extends Mage_Adminhtml_Block_Template
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    public function __construct()
    {
        $this->setTemplate('rjs/subregistry/customer/main.phtml');
        parent::_construct();
    }
    public function getCustomerId()
    {
        return Mage::registry('current_customer')->getId();
    }

    public function getTabLabel()
    {
        return $this->__('Zestawienie listy produktów');
    }
    public function getTabTitle()
    {
        return $this->__('Kliknij, aby uzyskać listy produktów wybranego klienta');
    }

    public function canShowTab()
    {
        return true;
    }
    public function isHidden()
    {
        return false;
    }
}