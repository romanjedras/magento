<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 2017-10-05
 * Time: 20:51
 */
class Rjs_Subregistry_Block_Adminhtml_Registries_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct(){
        parent::__construct();
        $this->setId('registriesGrid');
        $this->setDefaultSort('added_at');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection(){
        $collection = Mage::getModel('rjs_subregistry/subscribe')->getCollection();
        $collection->getSelect()->join(
            'customer_entity',
// note this join clause!
            'main_table.subscribe_email = customer_entity.email',
            array('entity_id')
        );
        $collection->getSelect()->join(
            'catalog_product_entity',
            // note this join clause!
            'main_table.product_id = catalog_product_entity.entity_id',
            array('type_id','sku')
        );

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $helper = Mage::helper('rjs_subregistry');
        $customerList = Mage::getModel('customer/customer')->load($this->getRequest()->getParam('id'));


        $this->addColumn('subscribe_id', array(
            'header' => Mage::helper('rjs_subregistry')->__('Id'),
            'width' => '10',
            'index' => 'subscribe_id',
            'sortable' => false,
        ));

        $this->addColumn('subscribe_email', array(
            'header' => Mage::helper('rjs_subregistry')->__('User email'),
            'width' => '10',
            'index' => 'subscribe_email',
            'sortable' => false,
        ));
        $this->addColumn('entity_id', array(
            'header' => Mage::helper('rjs_subregistry')->__('User Id'),
            'width'=>'10',
            'type'      => 'text',
            'name'      => 'entity_id',
            'align'     => 'center',
            'index'     => 'entity_id'
        ));
        $this->addColumn('product_id', array(
            'header' => Mage::helper('rjs_subregistry')->__('Product Id'),
            'width'=>'10',
            'type'      => 'text',
            'name'      => 'product_id',
            'align'     => 'center',
            'index'     => 'product_id'
        ));
        $this->addColumn('sku', array(
            'header' => Mage::helper('rjs_subregistry')->__('Product SKU'),
            'width'=>'10',
            'type'      => 'text',
            'name'      => 'sku',
            'align'     => 'center',
            'index'     => 'sku'
        ));
        $this->addColumn('type_id', array(
            'header' => Mage::helper('rjs_subregistry')->__('Product type'),
            'width'=>'10',
            'type'      => 'text',
            'name'      => 'type_id',
            'align'     => 'center',
            'index'     => 'type_id'
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction(){
    }

}