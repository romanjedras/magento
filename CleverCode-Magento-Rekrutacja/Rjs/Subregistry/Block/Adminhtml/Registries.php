<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 2017-10-05
 * Time: 20:46
 */
class Rjs_Subregistry_Block_Adminhtml_Registries extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct(){
        $this->_controller = 'adminhtml_registries';
        $this->_blockGroup = 'rjs_subregistry';
        $this->_headerText = Mage::helper('rjs_subregistry')->__('Menedżer listy produktów');
        parent::__construct();
    }
}