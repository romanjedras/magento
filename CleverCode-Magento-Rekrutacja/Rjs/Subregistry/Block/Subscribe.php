<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 2017-10-05
 * Time: 09:30
 */
class Rjs_Subregistry_Block_Subscribe extends Mage_Core_Block_Template
{
    public function getSubscribeRegistries()
    {
        $collection = null;
        $currentCustomer = Mage::getSingleton('customer/session')->getCustomer();

        if($currentCustomer) {
            $collection = Mage::getModel('rjs_subregistry/subscribe')->getCollection()
                ->addFieldToFilter('subscribe_email', array('email' => 'roman.j@clevercode.pl'));
        }
        return $collection;
    }

    public function getCustomer()
    {

        $collection = null;
        $currentCustomer = Mage::getSingleton('customer/session')->getCustomer();

        if($currentCustomer) {
            $collection = Mage::getModel('rjs_subregistry/subscribe')->getCollection()
                ->addFieldToFilter('subscribe_email', $currentCustomer->getEmail());
        }
        return $collection;
    }
}