<?php
$installer = $this;
$installer->startSetup();
/**
 * Create Registry Type Table
 *
 *
 */

$tableName = $installer->getTable('rjs_subregistry/subscribe');
// Check if the table already exists
if ($installer->getConnection()->isTableExists($tableName) != true) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('rjs_subregistry/subscribe'))
        ->addColumn('subscribe_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'Subscribe Id')
        ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable'  => false
        ), 'Product Id')
        ->addColumn('subscribe_email', Varien_Db_Ddl_Table::TYPE_TEXT, 255,
            array(),
            'Subscribe Email'
        )
        ->addColumn('added_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null,
            array(
                'nullable' => false,
            ),
            'Added At')
        ->addIndex($installer->getIdxName('rjs_subregistry/subscribe', array('product_id')),
            array('product_id'));

    $installer->getConnection()->createTable($table);
}

$installer->endSetup();